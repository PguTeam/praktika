#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void quicksort(int *arr, int left, int right) {
	int elem = arr[left];
	int l_hold = left;
	int r_hold = right;

	while (left < right) {
		while (arr[right] >= elem && left < right)
			right--;
		if (left != right) {
			arr[left] = arr[right];
			left++;
		}

		while (arr[left] <= elem && left < right)
			left++;
		if (left != right) {
			arr[right] = arr[left];
			right--;
		}
	}

	arr[left] = elem;
	elem = left;
	left = l_hold;
	right = r_hold;

	if (left < elem)
		quicksort(arr, left, elem - 1);
	if (right > elem)
		quicksort(arr, elem + 1, right);
}

void print(int *arr, int size) {
	if (size > 20)
		return;

	for (int i = 0; i < size; i++)
		printf("%d ", arr[i]);
	printf("\n");
}

void write(int *arr, int size, char *path) {
	FILE *file = fopen(path, "w");

	if (file) {
		printf("saving %s... ", path);
		for (int i = 0; i < size; i++)
			fprintf(file, "%d ", arr[i]);

		fclose(file);
		printf("ok\n");
	} else {
		printf("save error\n");
		exit(1);
	}
}

void read(int *arr, int size, char *path) {
	FILE *file = fopen(path, "r");
	if (file) {
		printf("reading %s... ", path);
		for (int i = 0, buf = 0; fscanf(file, "%d", &buf) != EOF; i++)
			*(arr + i) = buf;

		fclose(file);
		printf("ok\n");
	} else {
		printf("read error\n");
		exit(1);
	}
}
int main() {
	setbuf(stdout, NULL);
	srand(time(NULL));

	char input[] = "input.txt";
	char output[] = "output.txt";

	int size = 0;
	printf("enter array size: ");
	scanf("%d", &size);

	int *array = malloc(sizeof(int) * size);
	for (int i = 0; i < size; i++)
		array[i] = rand() % 2000 + 1000;
	write(array, size, input);

	free(array);
	array = malloc(sizeof(int) * size);
	read(array, size, input);

	printf("sorting... ");
	quicksort(array, 0, size - 1);

	printf("done\n");

	write(array, size, output);

	free(array);
	getchar();

	return 0;
}

